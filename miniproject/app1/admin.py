from django.contrib import admin
from .models import Machine , Sale, Product
# Register your models here.

admin.site.register(Machine)
admin.site.register(Sale)
admin.site.register(Product)

