from django.db import models
import uuid
# Create your models here.

class Product(models.Model):
    product_name = models.CharField(null=True, blank=True, max_length=200)
    code = models.IntegerField(null=True, blank=True, default=0)
    created = models.DateTimeField(auto_now_add=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                            primary_key=True,editable=False)

    def __str__(self):
        return self.product_name 

class Machine(models.Model):
    machine_name = models.CharField(null=True, blank=True, max_length=200)
    street = models.CharField(null=True, blank=True, max_length=200)
    products = models.ManyToManyField(Product, through='Sale')
    created = models.DateTimeField(auto_now_add=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                            primary_key=True,editable=False)

    def __str__(self):
        return self.machine_name 


class Sale(models.Model):
    machine = models.ForeignKey(Machine, on_delete=models.CASCADE, null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, null=True)
    price = models.IntegerField(null=True, blank=True, default=0)
    created = models.DateTimeField(auto_now_add=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                            primary_key=True,editable=False)

    def __str__(self):
        return self.product.product_name

class MachineView(models.Model):
    id_machine = models.ForeignKey(Machine,on_delete=models.CASCADE, null=True)
    id_product= models.ForeignKey(Product,on_delete=models.CASCADE, null=True)
    id_sale = models.ForeignKey(Sale,on_delete=models.CASCADE, null=True)
    created = models.DateTimeField(auto_now_add=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                            primary_key=True,editable=False)

    def __str__(self):
        return self.id

