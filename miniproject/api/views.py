from django.http import HttpResponse 
from rest_framework.decorators import api_view
from rest_framework.response import Response 


from .serializers import MachineSerializer, ProductSerializer,SalesSerializer 
from app1.models import Machine , Product, Sale
# from api import serializers



def test(request):
    return HttpResponse('hello')

# Machine
@api_view(['GET','POST'])
def machine_list(request):
    if request.method == 'GET':
        machines = Machine.objects.all()
        serializer = MachineSerializer(machines, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = MachineSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors)


@api_view(['GET','PUT','DELETE'])
def machine_detail(request,pk):
    try:
        machine = Machine.objects.get(id=pk)
    except:
        return Response("Can't find ID ")

    if request.method == 'GET':
        serializer = MachineSerializer(machine,many=False)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = MachineSerializer(machine, data =request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.errors)

    elif request.method == "DELETE":
        machine.delete()
        return Response('Machine was delete')

# Product 
@api_view(['GET','POST'])
def product_list(request):
    if request.method == 'GET':
        products = Product.objects.all()
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors)
        

@api_view(['GET','PUT','DELETE'])
def product_detail(request,pk):
    try:
        product = Product.objects.get(id=pk)
    except:
        return Response("Can't find ID ")

    if request.method == 'GET':
        serializer = ProductSerializer(product,many=False)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ProductSerializer(product, data =request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.errors)

    elif request.method == "DELETE":
        product.delete()
        return Response('Machine was delete')

# Sale 
@api_view(['GET','POST'])
def sale_list(request):
    if request.method == 'GET':
        sales= Sale.objects.all()
        serializer = SalesSerializer(sales, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = SalesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)

        return Response(serializer.errors)


@api_view(['GET','PUT','DELETE'])
def sale_detail(request,pk):
    try:
        sale= Sale.objects.get(id=pk)
    except:
        return Response("Can't find ID")

    if request.method == 'GET':
        serializer = SalesSerializer(sale,many=False)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = SalesSerializer(sale, data =request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.errors)

    elif request.method == "DELETE":
        sale.delete()
        return Response('Sale was delete')

