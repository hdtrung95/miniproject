from django.urls import path 
from . import views 

urlpatterns = [ 
    path('test', views.test, name='test'),
    path('machines', views.machine_list, name='machines_list'),
    path('machine/<str:pk>', views.machine_detail, name='machine'),
    path('products',views.product_list,name='product_list'),
    path('product/<str:pk>', views.product_detail, name='product'),
    path('sales',views.sale_list,name='sales_list'),
    path('sale/<str:pk>', views.sale_detail, name='sale'),
]
